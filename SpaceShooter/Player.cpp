#include "Player.h"
#include "Bullet.h"

Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer): sprite(playerTexture)
, velocity(0.0f, 0.0f)
, speed(300.0f)
, screenBounds(newScreenBounds)
, bullets(newBullets)
, bulletTexture(newBulletTexture)
, bulletCooldownRemaining(sf::seconds(0.0f))
, bulletCooldownMax(sf::seconds(0.5f))
, bulletFireSound(firingSoundBuffer)
{
	
	Reset(screenBounds);

}

void Player::Input()
{
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{

		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		bullets.push_back(Bullet(bulletTexture, screenBounds, bulletPosition, sf::Vector2f(1000, 0)));

		// Play firing sound
		bulletFireSound.play();
	}
}

void Player::Update(sf::Time frameTime)
{

	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

    // Checks if left of our sprite is off screen
	if (newPosition.x < 0)
		newPosition.x = 0;

	// Checks if right of our sprite is off screen
	if (newPosition.x + sprite.getTexture()->getSize().x > screenBounds.x)
		newPosition.x = screenBounds.x - sprite.getTexture() -> getSize().x;	

	// Checks if left of our sprite is off screen
	if (newPosition.y < 0)
		newPosition.y = 0;

	// Checks if right of our sprite is off screen
	if (newPosition.y + sprite.getTexture()->getSize().y > screenBounds.y)
		newPosition.y = screenBounds.y - sprite.getTexture()->getSize().y;

	// Move the player
	sprite.setPosition(newPosition);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && bulletCooldownRemaining <= sf::seconds(0.0f))
	{

		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		bullets.push_back(Bullet(bulletTexture, screenBounds, bulletPosition, sf::Vector2f(1000, 0)));

		bulletCooldownRemaining = bulletCooldownMax;

	}

}

void Player::Reset(sf::Vector2u screenBounds)
{
	sprite.setPosition(screenBounds.x / 2 - sprite.getTexture()->getSize().x / 2, screenBounds.y / 2 - sprite.getTexture()->getSize().y / 2);

}
void Player::Draw(sf::RenderWindow& window)
{

	window.draw(sprite);

}
