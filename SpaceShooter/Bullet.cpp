#include "Bullet.h"

Bullet::Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPosition, sf::Vector2f newVelocity)
{
	sprite.setTexture(bulletTexture);

	screenBounds = newScreenBounds;

	sprite.setPosition(startingPosition);

	velocity = newVelocity;

	alive = true;
}

bool Bullet::GetAlive()
{

	return alive;

}

void Bullet::Update(sf::Time frameTime)
{
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	// Hvae we gone off the screeen completely to the left or right?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0 || newPosition.x > screenBounds.x)
	{

		alive = false;

	}

	sprite.setPosition(newPosition);
}

void Bullet::DrawTo(sf::RenderTarget& target)
{

	target.draw(sprite);

}
