#pragma once

// Library needed for using sprites, texures and fonts
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

// Library for handling collections of objects
#include <vector>

class Player
{
	// Access level
public:

	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer);
    

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);
	void Draw(sf::RenderWindow& window);

private:
	// Variables used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenBounds;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;

};

