// Including libraries
#include <SFML/Graphics.hpp>

// include our own class definitions
#include "Player.h"
#include "Star.h"
#include "Bullet.h"

int main()
{

	sf::RenderWindow window(sf::VideoMode(800, 800), "Space Shooter");



     // ========= Game Setup ========= //

	// Bullets
	std::vector<Bullet> bullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");

	// Player 

	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	Player playerObject(playerTexture, window.getSize(), bullets);


	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, window.getSize()));
	}
	// Game Clock
	sf::Clock gameClock;

	while (window.isOpen())
	{

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		// ========= Update Section ========= // 

		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		// Player keybind input
		playerObject.Input();

		// Moves the player
		playerObject.Update(frameTime);

		for (int i = bullets.size() - 1; i >= 0; --i)
		{

			bullets[i].Update(frameTime);

			// If the bullet is dead, delete it
			if (!bullets[i].GetAlive())
			{

				// Remove the item from the vector
				bullets.erase(bullets.begin() + i);

			}

		}

		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].Update(frameTime);
		}

		// ========= Draw Section ========== //

		window.clear(sf::Color::Black);
			
		for (int i = 0; i < stars.size(); ++i) 
		{
			stars[i].DrawTo(window);
		}
		
		playerObject.Draw(window);

		window.display();

	}



	return 0;
}